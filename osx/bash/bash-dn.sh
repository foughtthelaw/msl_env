#! /bin/bash

BASHDIR="/Users/$USER/repos/msl_env/osx/bash"

cat "${BASHDIR}/bashrc.txt"> ~/.bashrc 
cat "${BASHDIR}/bash_profile.txt" > ~/.bash_profile 

echo '.bashrc and .bash_profile have been updated from REPOS.'  