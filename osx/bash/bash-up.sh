#! /bin/bash

BASHDIR="/Users/$USER/repos/msl_env/osx/bash"

cat ~/.bashrc > "${BASHDIR}/bashrc.txt"
cat ~/.bash_profile > "${BASHDIR}/bash_profile.txt"

echo '.bashrc and .bash_profile have been pushed'