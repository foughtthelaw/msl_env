#   -------------------------------
#   0. SETUP AND RANDO BULLSHIT
#   -------------------------------


# Updates bash profile alias file (this file)
alias bas="source ~/.bash_profile && echo '.bash_profile updated'"

#   -------------------------------
#   1.MAKE TERMINAL BETTER
#   -------------------------------

export PS1="\[\033[38;5;180m\]\w\[$(tput sgr0)\]\[\033[38;5;15m\]\n\[$(tput sgr0)\]\[\033[38;5;183m\]\t\[$(tput sgr0)\]\[\033[38;5;15m\] [\[$(tput sgr0)\]\[\033[38;5;183m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\]@\[$(tput sgr0)\]\[\033[38;5;183m\]\h\[$(tput sgr0)\]\[\033[38;5;15m\]] \\$\[$(tput sgr0)\]"


alias ll='ls -FGlhp'
alias la='ls -FGlAhp'						# Better list function

alias ..='cd ../'
alias ...='cd ../../'                       # Go back 2 directory levels
alias .3='cd ../../../'                     # Go back 3 directory levels
alias .4='cd ../../../../'                  # Go back 4 directory levels
alias .5='cd ../../../../../'               # Go back 5 directory levels
alias .6='cd ../../../../../../'            # Go back 6 directory levels

alias cp='cp -ipvR'
alias rsync='rsync -ahP'
alias mv='mv -iv'
alias mkdir='mkdir -pv'
mcd () { mkdir -p "$1" && cd "$1"; }        # mcd: Makes new Dir and jumps inside
alias less='less -FSRXc'  
alias c='clear'
alias f='open -a Finder ./'                 # Open current dir
alias rm='rm -rvf'
alias cwd='pwd | pbcopy'                    # Copy Working Directory
alias df='df -lH'
alias tail='tail -n 0 -f '                  # Sets tail default behavior to show last line of doc as added
alias nukeit='rm -rv *'						# be fucking careful with this one
alias rmds="find . -name .DS_Store -exec rm -v {} \;"
alias dot_clean="find . -type d | xargs dot_clean -m"
alias which='type -all'                     # which:        Find executables
alias path='echo -e ${PATH//:/\\n}'         # path:         Echo all executable Paths

#   -------------------------------
#   2. FILE AND FOLDER MANAGEMENT
#   -------------------------------

alias numFiles='echo $(ls -1 | wc -l)'      # numFiles:     Count of non-hidden files in current dir

#   cdf:  'Cd's to frontmost window of MacOS Finder
#   ------------------------------------------------------
cdf () {
    currFolderPath=$( /usr/bin/osascript <<EOT
        tell application "Finder"
            try
        set currFolder to (folder of the front window as alias)
            on error
        set currFolder to (path to desktop folder as alias)
            end try
            POSIX path of currFolder
        end tell
EOT
        )
        echo "cd to \"$currFolderPath\""
        cd "$currFolderPath"
    }

#   -------------------------------
#   4. Git Optimization
#   -------------------------------

alias gs='git status'
alias gc='git commit -m'
alias gco='git checkout'
alias ga='git add --all'
alias gl='git pull'
alias gp='git push'
