#! /bin/bash

DATE=`date +%Y%m%d`
NEWDIR="$DATE-$1"

if [ ! -d "$NEWDIR" ]; then
    mkdir -p "$NEWDIR/src/"
    mkdir -p "$NEWDIR/export/jpg/"
    mkdir -p "$NEWDIR/lrcat/"
    mkdir -p "$NEWDIR/psd/"
else
    echo "This project already exists."
fi

open $NEWDIR