#! /bin/bash

#######################################################
#
#   Copies AE Template Project and renames to first arg
#
#######################################################

# Set as path to master project template
AE_PRJ_TMP="/Volumes/GoogleDrive/Shared drives/Lyft-Motion/ae/templates/video_project"

echo "the template folder is ${TEMPLATE_NAME}"
cp -iR "${AE_PRJ_TMP}" .
mv -i 'video_project' $1
echo "$1 is ready for use. Now get to work"
open $1
