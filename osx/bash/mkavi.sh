#! /bin/bash

infile=$1

tempfile="${infile##*/}"
 
ffmpeg -i $infile -acodec copy -vcodec copy "${tempfile%.*}".avi
