#!/bin/bash
# Get the users attention so they don't sit around wondering what's happening.

# osascript -e 'tell app "System Events" to display dialog "Please follow the instructions in the Terminal window and provide your password when asked." buttons {"Lets do this!"}'


#############################################
#### Homeberw and other Package handlers ####
#############################################

# Install Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew update

# Install various cmd-line tools via Homebrew then NPM
brew install imagemagick ffmpeg gifsicle python3 node youtube-dl aria2 htop unrar
pip install --upgrade pip
pip3 install pylint dropbox clipboard ffmpeg-python Pillow requests pygifsicle
npm install -g imageoptim-cli tree-cli


##############################################
################ Update RSYNC ################
##############################################

cd ~/Desktop
curl -O http://rsync.samba.org/ftp/rsync/src/rsync-3.1.3.tar.gz
tar -xzvf rsync-3.1.3.tar.gz
rm rsync-3.1.3.tar.gz

cd ~/Desktop/rsync-3.1.3
./prepare-source
./configure
make
sudo make install

cd ..
rm -rf ~/Desktop/rsync-3.1.3

##############################################
#### download packages for manual Install ####
##############################################

mkdir ~/Desktop/INSTALL-ME/
cd ~/Desktop/INSTALL-ME/

# Create _READ-ME.txt and open INSTALL-ME folder
touch _READ-ME.txt
echo "Wait's over! Now you have the enviable task of installing all the apps we couldn't do for you. You can find the files you need in $PWD" > _READ-ME.txt
echo " " >> _READ-ME.txt
echo "The following apps are available:" >> _READ-ME.txt
echo "" >> _READ-ME.txt

### STAND ALONE APPS ###

mkdir standAloneApps
cd standAloneApps

APP="ImageOptim"
echo "Downloading $APP for manual install."
echo "$APP" >> ../_READ-ME.txt
aria2c -x4 https://imageoptim.com/ImageOptim.tbz2

APP="ImageAlpha"
echo "Downloading $APP for manual install."
echo "$APP" >> ../_READ-ME.txt
aria2c -x4 https://pngmini.com/ImageAlpha1.5.1.tar.bz2

APP="FreeFileSync"
echo "Downloading $APP for manual install."
echo "$APP" >> ../_READ-ME.txt
aria2c -x4 https://freefilesync.org/download/FreeFileSync_10.4_macOS.zip

<<<<<<< HEAD
# APP="Miro Video Converter"
=======
APP="Miro Video Converter"
echo "Downloading $APP for manual install."
echo "$APP" >> ../_READ-ME.txt
aria2c -x4 http://getmiro.com/files/MiroVideoConverter.dmg

cd ..

### AE PLUGINS ###

# mkdir aeplugins
# cd aeplugins
# echo "" >> ../_READ-ME.txt
# echo "The following After Effects plugins are available:" >> ../_READ-ME.txt
# echo "" >> ../_READ-ME.txt


# APP="VideoCopilot Saber"
# echo "Downloading $APP for manual install."
# echo "$APP" >> ../_READ-ME.txt
# aria2c -x4 http://vcproducts.s3.amazonaws.com/free/saber/SaberInstaller_1.0.39_Mac_2018.zip

# APP="Mekajiki Unmult"
>>>>>>> a4d6c0e1a25c39cae33b95b1b7a195ec07f5888e
# echo "Downloading $APP for manual install."
# echo "$APP" >> ../_READ-ME.txt
# aria2c -x4 http://getmiro.com/files/MiroVideoConverter.dmg

<<<<<<< HEAD
cd ..
=======
# cd .. 
>>>>>>> a4d6c0e1a25c39cae33b95b1b7a195ec07f5888e

open .
open _READ-ME.txt
exit


##############################################
############ New App Template ################
##############################################
# APP=""
# echo "Downloading $APP for manual install."
# echo "$APP" >> _READ-ME.txt
# curl -# -o ./FILENAMEHERE.EXT "PATH TO THE ACTUAL DOWNLOAD"