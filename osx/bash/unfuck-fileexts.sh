#!/bin/bash

# After Effects takes ownership of .json files and Premiere takes ownership of .csv and jsx files.  You
# can easily set this back manually but I suspect this will happen with every update, so
# let's not bother changing N^x system wide prefs manually every time.

# brew install duti

duti -s com.microsoft.VSCode csv all
duti -s com.microsoft.VSCode json all
duti -s com.microsoft.VSCode md all
duti -s com.microsoft.VSCode sh all
duti -s com.microsoft.VSCode lua all
duti -s com.microsoft.VSCode rst all
duti -s com.microsoft.VSCode py all
duti -s com.microsoft.VSCode sh all
duti -s com.microsoft.VSCode txt all
duti -s com.microsoft.VSCode xml all
duti -s com.microsoft.VSCode js all
duti -s com.microsoft.VSCode jsx all
duti -s com.microsoft.VSCode cfg all