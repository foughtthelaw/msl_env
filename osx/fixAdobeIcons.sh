#! /bin/bash

RUN_DIR=`dirname "$0"`

echo "NOTE: Updating app icons requires admin access."
echo "This will OVERWRITE your existing adobe app icons and cannot be undone."
echo "If you wish to cancel close the window and terminate the process."
echo ""
sudo -S echo ""
echo ""
echo "Installing the De-Re-Branded Adobe CC icons."
echo ""
echo ""


CUR_APP="/Applications/Adobe After Effects 2020/Adobe After Effects 2020.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating After Effects"
    sudo -S cp "$RUN_DIR/Ae.icns" "$CUR_APP/Contents/Resources/ae_app_stable.icns"
fi

CUR_APP="/Applications/Adobe Animate 2020/Adobe Animate 2020.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Animate"
    sudo -S cp "$RUN_DIR/An.icns" "$CUR_APP/Contents/Resources/appIcon.icns"
fi

CUR_APP="/Applications/Adobe Audition 2020/Adobe Audition 2020.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Audition"
    sudo -S cp "$RUN_DIR/Au.icns" "$CUR_APP/Contents/Resources/au_appicon.icns"
fi

CUR_APP="/Applications/Adobe Bridge 2020/Adobe Bridge 2020.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Bridge"
    sudo -S cp "$RUN_DIR/Br.icns" "$CUR_APP/Contents/Resources/bridge.icns"
fi

CUR_APP="/Applications/Adobe Character Animator 2020/Adobe Character Animator 2020.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Character Animator"
    sudo -S cp "$RUN_DIR/Ch.icns" "$CUR_APP/Contents/Resources/appIcon.icns"
fi

CUR_APP="/Applications/Adobe Dimension/Adobe Dimension.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Dimension"
    sudo -S cp "$RUN_DIR/Dn.icns" "$CUR_APP/Contents/Resources/dn_appicon.icns"
fi

CUR_APP="/Applications/Adobe Dreamweaver 2020/Adobe Dreamweaver 2020.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Dreamweaver"
    sudo -S cp "$RUN_DIR/Dw.icns" "$CUR_APP/Contents/Resources/Dreamweaver.icns"
fi

CUR_APP="/Applications/Adobe Illustrator 2020/Adobe Illustrator.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Illustrator"
    sudo -S cp "$RUN_DIR/Ai.icns" "$CUR_APP/Contents/Resources/ai_cc_appicon.icns"
fi

CUR_APP="/Applications/Adobe InCopy 2020/Adobe InCopy 2020.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating InCopy"
    sudo -S cp "$RUN_DIR/Ic.icns" "$CUR_APP/Contents/Resources/IC_App_Icon.icns"
fi

CUR_APP="/Applications/Adobe InDesign 2020/Adobe InDesign 2020.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating InDesign"
    sudo -S cp "$RUN_DIR/Id.icns" "$CUR_APP/Contents/Resources/ID_App_Icon.icns"
fi

CUR_APP="/Applications/Adobe Lightroom CC/Adobe Lightroom.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Lightroom CC"
    sudo -S cp "$RUN_DIR/Lr.icns" "$CUR_APP/Contents/Resources/lr_appicon.icns"
fi

CUR_APP="/Applications/Adobe Lightroom Classic/Adobe Lightroom Classic.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Lightroom Classic"
    sudo -S cp "$RUN_DIR/LrC.icn"s "$CUR_APP/Contents/Resources/App.icns"
fi

CUR_APP="/Applications/Adobe Media Encoder 2020/Adobe Media Encoder 2020.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Media Encoder"
    sudo -S cp "$RUN_DIR/ME-Lega"cy.icns "$CUR_APP/Contents/Resources/ame_appicon.icns"
fi

CUR_APP="/Applications/Adobe Photoshop 2020/Adobe Photoshop 2020.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Photoshop"
    sudo -S cp "$RUN_DIR/Ps.icns" "$CUR_APP/Contents/Resources/PS_AppIcon.icns"
fi

CUR_APP="/Applications/Adobe Premiere Pro 2020/Adobe Premiere Pro 2020.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Premiere Pro"
    sudo -S cp "$RUN_DIR/Pr.icns" "$CUR_APP/Contents/Resources/pr_app_icons.icns"
fi

CUR_APP="/Applications/Adobe Premiere Rush/Adobe Premiere Rush.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Premiere Rush"
    sudo -S cp "$RUN_DIR/Ru.icns" "$CUR_APP/Contents/Resources/rush_app_icons.icns"
fi

CUR_APP="/Applications/Adobe XD/Adobe XD.app"
if [ -d "$CUR_APP" ]
then
    echo "Updating Xd"
    sudo -S cp "$RUN_DIR/Xd.icns" "$CUR_APP/Contents/Resources/xd_appicon.icns"
fi


# TEMPLATE For Adding more APPS
# CUR_APP="PATH_TO_APP"
# if [ -d "$CUR_APP" ]
# then
#     echo "Updating "
#     sudo -S cp "$RUN_DIR/Pr.icns" "$CUR_APP/Contents/Resources/PATH_TO_ICNS_FILE"
# fi

echo ""
echo "Icon's are updated" 
echo ""
echo ""
echo ""