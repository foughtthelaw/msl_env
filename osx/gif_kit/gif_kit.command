#!/bin/bash
# Get the users attention so they don't sit around wondering what's happening.

osascript -e 'tell app "System Events" to display dialog "Please follow the instructions in the Terminal window and provide your password when asked." buttons {"Lets do this!"}'

touch .bashrc .bash_profile
cat gif_kit_bash_profile.txt >> .bash_profile
cat gif_kit_bashrc.txt >> .bashrc


#############################################
#### Homeberw and other Package handlers ####
#############################################

# Install Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew update

# Install various cmd-line tools via Homebrew then NPM
brew install imagemagick ffmpeg gifsicle python3
pip install --upgrade pip
pip3 install --upgrade pip
pip3 install pylint dropbox clipboard ffmpeg-python Pillow requests pygifsicle


##############################################
################ Update RSYNC ################
##############################################

cd ~/Desktop
curl -O http://rsync.samba.org/ftp/rsync/src/rsync-3.1.3.tar.gz
tar -xzvf rsync-3.1.3.tar.gz
rm rsync-3.1.3.tar.gz

cd ~/Desktop/rsync-3.1.3
./prepare-source
./configure
make
sudo make install

cd ..
rm -rf ~/Desktop/rsync-3.1.3

