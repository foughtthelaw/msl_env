#   TODO
#         if resize
#             resize gif


import os, sys, glob, ffmpeg, shutil, argparse
from pathlib import Path
from datetime import datetime
from pygifsicle import optimize


def main():
    # CLI arguement handling
    parser = argparse.ArgumentParser(
        description="A quick script to convert all MOV files to animated gif's. Dialed to work w/ transparency")
    parser.add_argument('-i', '--input', type=str, default='*',
                        help='Path to file or directory. Defaults to current directory.')
    parser.add_argument('-f', '--fps', type=int, default=None,
                        help='adjust the framerate of the output gif. Must be a whole number.')
    parser.add_argument('-r', '--resize', type=int, default=None,
                        help='Resizes the images long edge. Value is in pixels. Must be a whole number.')
    parser.add_argument('-o', '--optimize', action="store_true",
                        help='Optimize with gifsilcle. Default is None.')
    args = parser.parse_args()
    sys.stdout.write(str(convert_to_gifs(args)))


def convert_to_gifs(args):

    inputs = glob.glob(args.input)
    inputs = limit_file_types(inputs)
    inputs.sort()

    tempdir = str(Path.home())+"/vid2gifs_temp"
    if os.path.exists(tempdir):
        shutil.rmtree(tempdir)

    os.mkdir(tempdir)
    outdir = make_datetime_folder('_gifs')

    
    for item in inputs:
        out_filename = os.path.splitext(item)[0]

        if hasAlpha(item):
            make_pngs(item, tempdir)
            pngs_to_gif(tempdir, out_filename)
        else:
            ffmpeg_mkgif(item, args.fps, args.resize,
                         os.getcwd(), out_filename)

        if args.optimize:
            print("\n"+("!------Optimizing with GIFSICLE------!\n"*3))
            shutil.copyfile(f"{out_filename}.gif", f"{out_filename}-o.gif")
            optimize(f"{out_filename}-o.gif")

        for gif in glob.glob("*.gif"):

            base, extension = os.path.splitext(gif)

            if os.path.exists(f"{outdir}/{gif}"):
                duplicate_counter = 1
                while os.path.exists(f"{outdir}/{base}_{duplicate_counter}{extension}"):
                    duplicate_counter += 1
                shutil.copy(gif, f"{outdir}/{base}_{duplicate_counter}{extension}")
            else:
                shutil.move(gif, outdir)

    # remove all temp files
    shutil.rmtree(tempdir)


def make_datetime_folder(prefix=''):
    '''Takes a string as a prefix and prepends it onto the current date and time. them makes a folder with that name and returns the full path'''
    now = str(datetime.now())
    now = now.replace('-', '').replace(' ', '-').replace(':', '').split('.')[0]
    newdir = (prefix+'-'+now)
    os.mkdir(newdir)
    return os.getcwd()+'/'+newdir


def limit_file_types(in_list):
    supported_formats = ['.avchd', '.avi', '.m4p', '.m4v', '.mov', '.mp2',
                         '.mp4', '.mpe', '.mpeg', '.mpg', '.mpv', '.ogg', '.qt', '.webm', '.wmv']
    out_list = []

    for i in in_list:
        if os.path.isfile(i) and os.path.splitext(i)[1] in supported_formats:
            out_list.append(i)
    return out_list


def hasAlpha(in_file):

    alpha_formats = ["rgb32", "pal8", "bgr32",
                     "rgb32_1", "bgr32_1", "yuva444p12le"]

    vid_metadata = probe_video(in_file)
    pix_fmt = vid_metadata['pix_fmt']

    if pix_fmt in alpha_formats:
        return True
    return False


def make_pngs(in_file, out_dir):
    input_no_spaces = in_file.replace(' ', '\ ')
    os.system(f"ffmpeg -i {input_no_spaces} -vsync 0 {out_dir}/out-%4d.png")


def pngs_to_gif(in_path, out_name):
    out_name = out_name.replace(' ', '\ ')
    print(f"\nExporting {out_name}.gif\n")
    os.system(
        f"convert -delay 4 -loop 0 -dispose background {in_path}/*.png {out_name}.gif")


def probe_video(filename):
    probe = ffmpeg.probe(filename)
    video_stream = next(
        (stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
    return video_stream


def ffmpeg_mkgif(i, f,  w, od, of):
    # If framerate is None (Default) then assign the framerate of the input file
    if f is None:
        f = probe_video(i)['r_frame_rate'].split('/')[0]
    # If width setting is None (Default) then assign the width of the input file
    if w is None:
        w = probe_video(i)['width']

    # PIP ffmpeg-python module version.
    (
        ffmpeg
        .input(i)
        .filter(filter_name='palettegen')
        .output(i+'-palettegen-1.png', pix_fmt='pal8')
        .run(overwrite_output=True)
    )

    (
        ffmpeg
        .filter([
            ffmpeg.input(i),
            ffmpeg.input(i+'-palettegen-1.png'),
        ],
            filter_name='paletteuse',
            dither='heckbert',
            new='False',
        )
        .filter('fps', f)
        .filter('scale', w, -1)
        .output(f"{od}/{of}.gif")
        .run()
    )
    os.remove(i+'-palettegen-1.png')


if __name__ == "__main__":
    main()
