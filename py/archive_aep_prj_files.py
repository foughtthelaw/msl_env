import os
import shutil
import glob
import fancy_text as ft
from pathlib import Path

cur_dir = os.getcwd()
archive_folder = "_archive"

def get_aeps(dir_to_sort):
    aeps = [f for f in os.listdir(dir_to_sort) if os.path.isfile(f) and f.endswith('.aep')]
    aeps.sort()

    return aeps

def safe_move(in_file, dest):
    if os.path.exists(f"{dest}/{in_file}"):
        i = 0
        while os.path.exists(f"{dest}/{in_file}_{i}"):
            i += 1
        shutil.move(in_file,f"{dest}/{in_file}_{i}")
    else:
        shutil.move(in_file,dest)


if not os.path.exists(f"{cur_dir}/{archive_folder}"):
    os.mkdir(archive_folder)

ame_files = [a for a in glob.glob("*AME") if os.path.isdir(a)]
log_files = [a for a in glob.glob("*Logs") if os.path.isdir(a)]

files = get_aeps(cur_dir)[:-1]

# print("\n".join(ame_files))
# print("\n".join(log_files))
for f in files:
    safe_move(f,f"{cur_dir}/{archive_folder}")

for f in ame_files:
    safe_move(f,f"{Path.home()}/.Trash")

for f in log_files:
    safe_move(f, f"{Path.home()}/.Trash")

ft.header_box(f"older AEP files now in './{archive_folder}'\nLog and AME files have been moved to Trash.")
