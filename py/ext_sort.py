import os 
import shutil

# empty lists
file_types = []

# Get all files in current directory
files = [f for f in os.listdir('.') if os.path.isfile(f) and str(f)[0] != "."]

# Loop through files and isolate file types
for f in files:
    f = str(f).split('.')[-1]
    if (f not in file_types):
        file_types.append(f)

file_types.sort()

# make directores for file_types
for f in file_types:
    os.mkdir(f)

# loop through files and move to dir matching extension
for f in files:
    ext = str(f).split('.')[-1]
    shutil.move(f, "./"+ext+"/"+f)