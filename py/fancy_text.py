def header_box(message, char='*', padding=3):
    '''attention grabbing box to wrap your text in.'''

    lines = message.split("\n")

    max_line_width = 0
    for l in lines:
        if len(l) > max_line_width:
            max_line_width = len(l)

    box_width = max_line_width + (padding*4)
    gap = " "*padding
    pad = char*padding

    print(char*box_width)
    for l in lines:
        length_diff = max_line_width - len(l)
        formatted_line = " "*(int(length_diff/2))+l+" "*(int(length_diff/2))
        
        if len(formatted_line) < max_line_width:
            formatted_line = " "*(int(length_diff/2))+l+" "+" "*(int(length_diff/2))
        
        print(pad+gap+formatted_line+gap+pad)
    print(char*box_width)
