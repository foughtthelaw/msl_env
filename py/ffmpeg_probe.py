import sys
import argparse
import ffmpeg

def main():
    # CLI arguement handling
    parser = argparse.ArgumentParser(
        description="a quick way to run ffprobe on any video file")
    parser.add_argument('-i', type=str, help='Path to file')
    args = parser.parse_args()
    sys.stdout.write(str(probe_video(args)))

def probe_video(args):
    if args.i:
        probe = ffmpeg.probe(args.i)
    else: 
        return print("\nProbe failed.\nPlease use '-i' to provide a file so I can probe it.\n")

    video_stream = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
    t = video_stream

    for k in t.keys():
        print(k+" :: "+str(t[k]))

if __name__ == "__main__":
    main()
