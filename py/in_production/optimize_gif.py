import os, sys, glob, shutil
from PIL import Image, ImageChops
import numpy as np


inFile = os.path.abspath(sys.argv[1])

# make temp dir
temp_dir = os.mkdir('optgif_temp')
# unpack gif
os.system(f"convert {inFile} optgif_temp/out_%04d.gif")
# find duplicate frames


def mse(imageA, imageB):
	# the 'Mean Squared Error' between the two images is the
	# sum of the squared difference between the two images;
	# NOTE: the two images must have the same dimension
	err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
	err /= float(imageA.shape[0] * imageA.shape[1])
	
	# return the MSE, the lower the error, the more "similar"
	# the two images are
	return err

def remove_duplicate_frames(in_dir):
    frames = os.listdir(in_dir)
    frames.sort()

    dupe_frames = []

    for f in range(0,len(frames)-1):
        
        # NEEDS WORK

        img1 = np.asarray(Image.open(f"{in_dir}/{frames[f]}"))
        img2 = np.asarray(Image.open(f"{in_dir}/{frames[f+1]}"))

        img_diff = mse(img1,img2)

        if img_diff < 3850.0:
            print(f"the next frame is the SAME  : {img_diff}")
        else:
            print(f"the next frame is DIFFERENT : {img_diff}")

        
        # if result:
        #     print("Pictures are the same")
        #     dupe_frames.append(f"{in_dir}/{frames[f+1]}")
        # else:
        #     print("Pictures are different")
    
    print("Duplicate Frames:"+'\n'.join(dupe_frames))   
    shutil.rmtree(in_dir)

remove_duplicate_frames('optgif_temp')
        

# identify frame sequences
# pack sequences to seq_gifs
# pack seq_gifs w/ modified delays
# rename output