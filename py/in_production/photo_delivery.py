
# '''
# loop through files
#     # create hero dir
#     # create preview and email folders
#     # export 2048 jpg to preview
#     # export 1024 png to email
#     compress both
#     # move input files to hero folder
# '''

import os
from PIL import Image

files = [f for f in os.listdir(os.getcwd())]
files.sort()
print("\n".join(files))

try:
    os.mkdir("hero")
except:
    pass
try:
    os.mkdir("preview")
except:
    pass    
try:
    os.mkdir("email")
except:
    pass

total_files = len(files)
file_counter = 1

for f in files:
    
    percent_complete = 100*(file_counter/total_files)
    
    print(f"{round(percent_complete,3)}% Complete")
    print(f"Processing file {file_counter} of {total_files}.")
    
    file_counter += 1
    
    if f.endswith(".jpg"):

        fn, fext = os.path.splitext(f)
        i1 = Image.open(f)
        i2 = Image.open(f)

        i1.thumbnail([2048,2048])
        i1.save(f"preview/{fn}.jpg", quality=80)

        i2.thumbnail([1024, 1024])
        i2.convert(mode='P', palette=Image.ADAPTIVE)
        i2.save(f"email/{fn}.png")

        os.rename(f, f"hero/{fn+fext}")
