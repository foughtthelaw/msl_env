import os, logging

def copy_dir_tree(new_location,start='.'):
    walk = os.walk(start)
    dirs = list(set([d[0] for d in walk if d[0] != '.']))
    new_dirs = []

    for d in dirs:
        nd = d.replace('./',f'./{new_location}/')
        os.makedirs(nd, exist_ok=True)
        new_dirs.append(nd)
    new_dirs.sort()
    print('\n'.join(new_dirs))

copy_dir_tree('test_new_dir')