import clipboard

def fixDbUrl(url):
    url = url.replace('dl=0','raw=1')
    print("Updated URL is now:\n"+url)
    clipboard.copy(url)

fixDbUrl(clipboard.paste())