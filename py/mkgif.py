import os
import sys
import glob
import shutil
import argparse
from datetime import datetime

# mkgif update using imagemagick instead of ffmpeg for the final gif creation


def main():
    # CLI arguement handling
    parser = argparse.ArgumentParser(
        description="A quick script to convert all MOV files to animated gif's. Dialed to work w/ transparency")
    parser.add_argument('-i','--input', type=str, default='',
                        help='Path to file or directory. Defaults to current directory.')
    parser.add_argument('-r','--resize', type=int, default=None,
                        help='Resizes the images long edge. Value is in pixels. Must be a whole number.')
    parser.add_argument('-o','--optimize', action="store_true",
                        help='Optimize with gifsilcle. Default is None.')
    parser.add_argument('-seq', '--sequence', action='store_true',
                        help='Flags input as an image sequence. Default is False')
    args = parser.parse_args()
    sys.stdout.write(str(make_gifs(args)))


def make_pngs(in_file, out_dir):
    input_no_spaces = in_file.replace(' ', '\ ')
    os.system(f"ffmpeg -i {input_no_spaces} -vsync 0 ./{out_dir}/out-%4d.png")


def png_to_gif(in_path, out_name, size, optimize=None):
    imgs = glob.glob(f"{in_path}/*.png")
    imgs.sort()

    if size:
        print("Resizing Image")
        os.mkdir(f'{in_path}/temp')
        for i in imgs:
            name_only = os.path.splitext(os.path.basename(i))[0]
            os.system(
                f"convert {i} -resize {size}x{size} {in_path}/temp/{name_only}.gif")

        print(f"Packing {in_path} into an animated GIF\n\n")
        os.system(
            f"convert -delay 4 -loop 0 -dispose background {in_path}/temp/*.gif {out_name}.gif")
        
        print(f"removing {in_path}/temp\n")
        shutil.rmtree(f'{in_path}/temp')
    else:
        print(f"Packing {in_path} into an animated GIF\n\n")
        os.system(
            f"convert -delay 4 -loop 0 -dispose background {in_path}/*.png {out_name}.gif")

    if optimize:
        print(f"Optimizing {out_name}.gif GIF\n\n")
        os.system(
            f"gifsicle -O3 --colors 256 {out_name}.gif -o {out_name}-o.gif")


def make_datetime_folder(prefix=''):
    '''Takes a string as a prefix and prepends it onto the current date and time. them makes a folder with that name and returns the full path'''
    now = str(datetime.now())
    now = now.replace('-', '').replace(' ', '-').replace(':', '').split('.')[0]
    newdir = (prefix+'-'+now)
    os.mkdir(newdir)
    return os.getcwd()+'/'+newdir


def get_seq_glob_name(filename):
    '''takes a path to a file from an image sequence, returns a list with filename w/o number at the end,  filename w/ glob pattern and the length of the sequence. meant to feed image sequences to ffmpeg. '''
    length = 0
    filename_only = os.path.basename(filename)
    basename_split = os.path.splitext(filename_only)

    fn = basename_split[0]
    ext = basename_split[1]

    rev = fn[::-1]
    if rev[0].isalpha():
        return False
    for s in rev:
        if s.isdigit():
            length += 1
        else:
            break
    
    if length >= 2:
        name_no_seq = fn[0:(len(fn)-length)]
        name_glob_seq = f"{name_no_seq}%0{length}d{ext}"
        return [name_no_seq, name_glob_seq, ext, length]
    return None

def make_gifs(args):
    if args.sequence:
        seq_to_gif(args)
    else:
        mov_to_gif(args)


def seq_to_gif(args):

    settings = {
        "input": str(args.input),
        "size": args.resize,
        "tempdir": "temp",
    }
    
    inputs = glob.glob(settings['input'])

    glob_info = get_seq_glob_name(inputs[0])
    
    out_filename = glob_info[0][:-1]
    in_path = os.path.split(inputs[0])[0]
    if args.optimize:
        png_to_gif(in_path, out_filename, settings['size'], args.optimize)
    else:
        png_to_gif(in_path, out_filename, settings['size'])

    out_folder = make_datetime_folder('gif')
    gifs = glob.glob("*.gif")
    for g in gifs:
        shutil.move(g, out_folder)


def mov_to_gif(args):

    settings = {
        "input": str(args.input),
        "size": args.resize,
        "tempdir": "temp_pngs",
    }

    inputs = glob.glob(settings['input'])

    for item in inputs:
        out_filename = os.path.splitext(os.path.basename(item))[0]
        this_temp = f"{settings['tempdir']}_{out_filename}"

        os.mkdir(f"./{this_temp}")
        make_pngs(item, this_temp)
        if args.optimize:
            png_to_gif(f"./{this_temp}", out_filename,
                       settings['size'], args.optimize)
        else:
            png_to_gif(f"./{this_temp}", out_filename, settings['size'])

        shutil.rmtree(f"./{this_temp}")

    out_folder = make_datetime_folder('gif')
    gifs = glob.glob("*.gif")
    for g in gifs:
        shutil.move(g, out_folder)


if __name__ == "__main__":
    main()
