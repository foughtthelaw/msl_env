import os
import sys
import argparse
from datetime import datetime


def main():
    # CLI arguement handling
    parser = argparse.ArgumentParser(
        description="A quick script to convert all MOV and MP4 files into optimized h264 encoded MP4s.")
    parser.add_argument('-i', type=str, default='.',
                        help='Path to file or directory. Defaults to current directory.')
    parser.add_argument('-a', type=str, default="-acodec aac -ab 96k",
                        help='Audio quality setting. (DEFAULT = -acodec aac -ab 96k)')
    parser.add_argument('-q', type=int, default=24,
                        help='(int: 1- 51) Sets the quality level. Lower numbers yield better images and bigger files.')
    parser.add_argument('-w', type=str, default='',
                        help='defines the width of the output video. height scaled to match input aspect ratio. default is source width')
    args = parser.parse_args()
    sys.stdout.write(str(make_mp4s(args)))


def header_box(message, char='*', padding=3):
    box_width = len(message) + (padding*4)
    gap = " "*padding
    pad = char*padding
    line = char*box_width
    print("\n\n")
    print(line)
    print(line)
    print(pad+gap+message+gap+pad)
    print(line)
    print(line)
    print("\n\n")



def remove_file_ext(path):
    '''strips the path and file extension from a path to a given file'''
    base = os.path.basename(path)
    fn_only = os.path.splitext(base)[0]
    return fn_only


def make_datetime_folder(prefix=''):
    '''Takes a string as a prefix and prepends it onto the current date and time. them makes a folder with that name and returns the full path'''
    now = str(datetime.now())
    now = now.replace('-', '').replace(' ', '-').replace(':', '').split('.')[0]
    newdir = (prefix+'-'+now)
    os.mkdir(newdir)
    return os.getcwd()+'/'+newdir


def get_all_sub_files_by_type(filetypes, starting_point='.'):
    '''Takes a list of strings of the file type and an optional starting point and returns a list of files with the stated type in their location relative to the starting point.'''
    files = []
    for dirpath, dirnames, filenames in os.walk(starting_point):
        for filename in [f for f in filenames if os.path.splitext(f)[1].replace('.', '') in filetypes]:
            files.append(os.path.join(dirpath, filename))
    return(files)


def make_mp4s(args):

    settings = {
        "input": args.i,
        "audio": args.a,
        "quality": args.q,
        "width": args.w,
        "out_dir": ''
    }

    # Longform ffmpeg command
    def run_ffmpeg(i, a, q, w, od):
        input_no_spaces = i.replace(' ', '\ ')
        os.system(
            f"ffmpeg -i {input_no_spaces} {a} -vcodec libx264 -pix_fmt yuv420p -f mp4 -crf {q} {w} -x264opts colorprim=bt709:transfer=bt709:colormatrix=bt709 -max_muxing_queue_size 1024 '{od}/{out_filename}.mp4'")

   # Create the new output foler and assign it to the settings dict
    output_prefix = '_mp4'
    settings['out_dir'] = make_datetime_folder(output_prefix)

    d = settings['out_dir']

    # Run immediately on individual files
    if os.path.isfile(settings['input']):

        out_filename = remove_file_ext(settings['input'])

        run_ffmpeg(settings['input'], settings['audio'],
                   settings['quality'], settings['width'], d)

    elif os.path.isdir(settings['input']):

        # Get list of all files in all subdirectories
        files = get_all_sub_files_by_type(['mp4', 'mov'])

        # Remove other directories previously created by this script
        files = [f for f in files if output_prefix not in f]

        # Create list of directories that need creatied. list(set()) to remove duplicates.
        dirs = list(set([thisDir.replace('.', d) for thisDir in [
            os.path.split(p)[0] for p in files]]))

        # Make ze directories.
        for thisDir in dirs:
            os.makedirs(thisDir, exist_ok=True)

        header_box('HERE COME THOSE TASTY FILES!!!')

        for f in files:
            # recreate foler structure in new output folder
            f_dir = os.path.split(f)[0].replace('./', '')
            cur_out_dir = f"{d}/{f_dir}"
            out_filename = remove_file_ext(f)

            # compress the individual files
            run_ffmpeg(f, settings['audio'],
                       settings['quality'], settings['width'], cur_out_dir)
    else:
        print('I\'m not sure how, but you managed to supply a bad input.\nDo better.')

    header_box('FILES ARE DONE. WE DID IT!!!')


if __name__ == "__main__":
    main()
