import os
# import pdb; pdb.set_trace()

def get_all_sub_files_by_type(filetypes,starting_point='.'):
    '''Takes a list of strings of the file type and an optional starting point and returns a list of files with the stated type in their location relative to the starting point.'''
    files = []
    for dirpath, dirnames, filenames in os.walk(starting_point):
        for filename in [f for f in filenames if os.path.splitext(f)[1].replace('.','') in filetypes]:
            files.append(os.path.join(dirpath, filename))
    return(files)


print('\n'.join(get_all_sub_files_by_type(['mp4','mov'])))