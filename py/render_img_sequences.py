import os
import sys
import argparse
import ffmpeg
from datetime import datetime


def main():
    # CLI arguement handling
    parser = argparse.ArgumentParser(
        description="Batch convert all image sequences in a given directory into preview mp4's")
    parser.add_argument('-i', type=str, default='.',
                        help='Path to file or directory. Defaults to current directory.')
    parser.add_argument('-f', type=int, default=24,
                        help='desired framerate for output. Default is 24')
    parser.add_argument('-q', type=int, default=24,
                        help='(int: 1- 51) Sets the quality level. Lower numbers yield better images and bigger files.')
    parser.add_argument('-w', type=str, default='',
                        help='defines the width of the output video. height scaled to match input aspect ratio. default is source width')
    args = parser.parse_args()
    sys.stdout.write(str(render_seqs(args)))


def make_datetime_folder(prefix=''):
    '''Takes a string as a prefix and prepends it onto the current date and time. them makes a folder with that name and returns the full path'''
    now = str(datetime.now())
    now = now.replace('-', '').replace(' ', '-').replace(':', '').split('.')[0]
    newdir = (prefix+'-'+now)
    os.mkdir(newdir)
    return os.getcwd()+'/'+newdir


def get_seq_glob_name(filename):
    '''takes a path to a file from an image sequence, returns a list with filename w/o number at the end,  filename w/ glob pattern and the length of the sequence. meant to feed image sequences to ffmpeg. '''
    length = 0
    filename_only = os.path.basename(filename)
    basename_split = os.path.splitext(filename_only)

    fn = basename_split[0]
    ext = basename_split[1]

    rev = fn[::-1]
    if rev[0].isalpha():
        return False
    for s in rev:
        if s.isdigit():
            length += 1
        else:
            break
    
    if length >= 2:
        name_no_seq = fn[0:(len(fn)-length)]
        name_glob_seq = f"{name_no_seq}%0{length}d{ext}"
        return [name_no_seq, name_glob_seq, ext, length]
    return None


def find_all_sequences(dir):
    '''walk's a dir and finds any file w/ numbers at the end of the name before the extension then builds and returns a list of glob patterened sequences'''
    seq_names = []
    seq_paths = []
    for root, dirs, files in os.walk(dir):
        for f in files:
            glob_info = get_seq_glob_name(f)
            if glob_info and glob_info[0] not in seq_names:
                seq_names.append(glob_info[0])
                seq_paths.append(root+'/'+glob_info[1])
    return list(zip(seq_paths, seq_names))


def probe_video(filename):
    probe = ffmpeg.probe(filename)
    video_stream = next(
        (stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
    return video_stream


def run_ffmpeg(in_file, fps, quality, w, od, of):
    input_no_spaces = in_file.replace(' ', '\ ')
    dup_counter = 1
    out_file = f'{od}/{of}.mp4'

    if w is None:
        w = probe_video(in_file)['width']
    if os.path.exists(out_file):
        out_file = f'{od}/{of}_{dup_counter}.mp4'
        dup_counter = dup_counter + 1
    (
        ffmpeg
        .input(input_no_spaces, framerate=fps)  # pattern_type='glob',
        # .filter('scale', w, -1)
        .output(out_file,  crf=quality, pix_fmt='yuv420p')
        .run()
    )


def render_seqs(args):

    settings = {
        'input': args.i,
        'fps': args.f,
        'quality': args.q,
        'width': args.w,
        "iFormats": ['.jpg', '.png', '.tif', '.tiff', '.psd'],      #'.exr', removed for compatability issues
        'out_dir': ''
    }

    settings['out_dir'] = make_datetime_folder('_preview')
    od = settings['out_dir']

    seqs = find_all_sequences(settings['input'])

    for f in seqs:
        filename_only = os.path.basename(f[0])
        basename_split = os.path.splitext(filename_only)
        if basename_split[1] in settings['iFormats']:
            run_ffmpeg(f[0], settings['fps'], settings['quality'],
                       settings['width'], od, f[1])


if __name__ == "__main__":
    main()
