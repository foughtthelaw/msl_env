import os


def get_seq_glob_name(filename):
    '''takes a path to a file from an image sequence, returns a list with filename w/o number at the end,  filename w/ glob pattern and the length of the sequence. meant to feed image sequences to ffmpeg. '''
    length = 0
    filename_only = os.path.basename(filename)
    basename_split = os.path.splitext(filename_only)

    fn = basename_split[0]
    ext = basename_split[1]

    rev = fn[::-1]
    if rev[0].isalpha():
        return False
    for s in rev:
        if s.isdigit():
            length += 1
        else:
            break

    name_no_seq = fn[0:(len(fn)-length)]
    name_glob_seq = f"{name_no_seq}%0{length}d{ext}"

    return [name_no_seq, name_glob_seq, ext, length]


def find_all_sequences(dir):
    '''walk's a dir and finds any file w/ numbers at the end of the name before the extension then builds and returns a list of glob patterened sequences'''
    seq_names = []
    seq_paths = []
    for root, dirs, files in os.walk(dir):
        for f in files:
            glob_info = get_seq_glob_name(f)
            if glob_info and glob_info[0] not in seq_names:
                seq_names.append(glob_info[0])
                seq_paths.append(root+'/'+glob_info[1])
    return list(zip(seq_paths, seq_names))


for f in find_all_sequences('.'):
    print(f)
